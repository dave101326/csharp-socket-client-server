﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace SocketServerUi
{
    public class BroadCast
    {
        /// <summary>
        /// broadcast msg to all sockets
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="uName">id_name</param>
        /// <param name="flag">true [time][id][msg], false: [time][msg]</param>
        /// <param name="clientList">list of sockets</param>
        public static void PushMessage(String msg, String uName, Boolean flag, Dictionary<String, Socket> clientList)
        {
            List<string> err_list = new List<string>();

            foreach (var item in clientList)
            {
                Socket brdcastSocket = (Socket)item.Value;
                String msgTemp = null;
                byte[] castBytes = new byte[4096];

                if (flag == true)
                {
                    msgTemp = string.Format("[{0}][{1}]{2}\r\n", DateTime.Now.ToString(), uName, msg);
                    castBytes = Encoding.UTF8.GetBytes(msgTemp);
                }
                else
                {
                    msgTemp = string.Format("[{0}]{1}\r\n", DateTime.Now.ToString(), msg);
                    castBytes = Encoding.UTF8.GetBytes(msgTemp);
                }

                try
                {
                    brdcastSocket.Send(castBytes);
                }
                catch (Exception)
                {
                    brdcastSocket.Close();
                    brdcastSocket = null;
                    err_list.Add(item.Key);
                    continue;
                }
            }

            foreach (var item in err_list)
            {
                clientList.Remove(item);
            }


        }
    }
}
