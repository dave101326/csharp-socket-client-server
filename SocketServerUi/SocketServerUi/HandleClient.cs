﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SocketServerUi
{
    public class HandleClient
    {
        Socket clientSocket;
        String clNo;
        Dictionary<String, Socket> clientList = new Dictionary<string, Socket>();
        TextBox txtMsg;
        public HandleClient() { }
        public HandleClient(TextBox tb)
        {
            txtMsg = tb;
        }
        public void StartClient(Socket inClientSocket, String clientNo, Dictionary<String, Socket> cList)
        {
            clientSocket = inClientSocket;
            clNo = clientNo;
            clientList = cList;

            Thread th = new Thread(Chat);
            th.IsBackground = true;
            th.Start();
        }
        private void Chat()
        {
            byte[] bytesFromClient = new byte[4096];
            String dataFromClient;
            bool isListen = true;

            while (isListen)
            {
                try
                {
                    if (clientSocket == null || !clientSocket.Connected)
                    {
                        return;
                    }

                    // 等待此socket之新的字串進來
                    if (clientSocket.Available > 0)
                    {
                        int len = clientSocket.Receive(bytesFromClient);
                        if (len > -1)
                        {
                            dataFromClient = Encoding.UTF8.GetString(bytesFromClient, 0, len);

                            if (!String.IsNullOrWhiteSpace(dataFromClient))
                            {
                                dataFromClient = dataFromClient.Substring(0, dataFromClient.LastIndexOf("$"));

                                if (!String.IsNullOrWhiteSpace(dataFromClient))
                                {
                                    BroadCast.PushMessage(dataFromClient, clNo, true, clientList);
                                    txtMsg.BeginInvoke( new Action( () => { txtMsg.AppendText(clNo + ":" + dataFromClient + "\r\n"); } ) );
                                }
                                else
                                {
                                    // client 結束時會發送 結束字串 [$]
                                    isListen = false;
                                    clientList.Remove(clNo);

                                    txtMsg.BeginInvoke(
                                        new Action(
                                            () => { txtMsg.Text += clNo + " has disconnected with server.\r\n"; }
                                        )
                                    );

                                    BroadCast.PushMessage(clNo + " offline\r\n", "", false, clientList);
                                    clientSocket.Close();
                                    clientSocket = null;
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    isListen = false;
                    clientList.Remove(clNo);

                    txtMsg.BeginInvoke(
                                        new Action(
                                            () => { txtMsg.Text += clNo + " has disconnected with server.\r\n"; }
                                        )
                                    );

                    BroadCast.PushMessage(clNo + " offline\r\n", "", false, clientList);

                    clientSocket.Close();
                    clientSocket = null;
                }
            }
        }
    }
}
