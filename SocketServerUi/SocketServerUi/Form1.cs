﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketServerUi
{
    public partial class Form1 : Form
    {
        public static Dictionary<String, Socket> clientList = null;
        Socket serverSocket = null;
        bool isListen = true;
        Thread thStartListen;
        IPAddress ipadr;
        IPEndPoint endPoint;

        public Form1()
        {
            InitializeComponent();
            ipadr = IPAddress.Loopback;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                clientList = new Dictionary<string, Socket>();
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                if (textBox2.Text != "")
                    ipadr = IPAddress.Parse(textBox2.Text.ToString().Trim());

                endPoint = new IPEndPoint(ipadr, 9487);

                try
                {
                    serverSocket.Bind(endPoint);
                    serverSocket.Listen(99);

                    thStartListen = new Thread(StartListen);
                    thStartListen.IsBackground = true;
                    thStartListen.Start();

                    SendMsg("Server: " + endPoint.Address.ToString() + ":9487");
                    SendMsg("Server start.");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                    SendMsg("Server start fail.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (serverSocket == null)
            {
                try
                {
                    isListen = true;
                    clientList = new Dictionary<string, Socket>();
                    serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    if (textBox2.Text != "")
                        ipadr = IPAddress.Parse(textBox2.Text.ToString().Trim());

                    endPoint = new IPEndPoint(ipadr, 9487);

                    try
                    {
                        serverSocket.Bind(endPoint);
                        serverSocket.Listen(99);

                        thStartListen = new Thread(StartListen);
                        thStartListen.IsBackground = true;
                        thStartListen.Start();

                        SendMsg("Server: " + endPoint.Address.ToString() + ":9487");
                        SendMsg("Server start.");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        SendMsg("Server start fail");

                        if (serverSocket != null)
                        {
                            serverSocket.Close();
                            thStartListen.Abort();

                            BroadCast.PushMessage("Server has closed.", "", false, clientList);
                            foreach (var socket in clientList.Values)
                            {
                                socket.Close();
                            }
                            clientList.Clear();

                            serverSocket = null;
                            isListen = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void StartListen()
        {
            isListen = true;
            Socket clientSocket = default(Socket);

            while(isListen)
            {
                try
                {
                    if (serverSocket == null)
                        return;
                    // 等待serverSocket接收新的client socket進來, 新的client進來後會發送id字串 [id$] 
                    clientSocket = serverSocket.Accept();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                byte[] bytesFrom = new byte[4096];
                string dataFromClient = string.Empty;

                if (clientList != null && clientSocket.Connected)
                {
                    try
                    {
                        int len = clientSocket.Receive(bytesFrom);

                        if (len > -1)
                        {
                            string tmp = Encoding.UTF8.GetString(bytesFrom, 0, len);
                            dataFromClient = tmp;

                            int subLen = dataFromClient.LastIndexOf("$");
                            if (subLen > -1)
                            {
                                // get name
                                dataFromClient = dataFromClient.Substring(0, subLen);
                                if (!clientList.ContainsKey(dataFromClient))
                                {
                                    clientList.Add(dataFromClient, clientSocket);

                                    BroadCast.PushMessage(dataFromClient + " Joined.", dataFromClient, false, clientList);

                                    HandleClient client = new HandleClient(textBox1);
                                    client.StartClient(clientSocket, dataFromClient, clientList);

                                    SendMsg(dataFromClient + " connect to the server.");
                                }
                                else
                                    clientSocket.Send(Encoding.UTF8.GetBytes("#" + dataFromClient + "#")); // 發現重複id, 回傳'#id#'給client, 不加入socket至broadList
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (serverSocket != null)
            {
                serverSocket.Close();
                thStartListen.Abort();

                BroadCast.PushMessage("Server has closed", "", false, clientList);
                foreach (var socket in clientList.Values)
                {
                    socket.Close();
                }
                clientList.Clear();

                serverSocket = null;
                isListen = false;
                SendMsg("Server stop. Close all connections.");
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serverSocket != null)
            {
                serverSocket.Close();
                thStartListen.Abort();

                BroadCast.PushMessage("Server has closed", "", false, clientList);
                foreach (var socket in clientList.Values)
                {
                    socket.Close();
                }
                clientList.Clear();

                serverSocket = null;
                isListen = false;
                SendMsg("Server stop. Close all connections.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SendMsg("Clean all.", true);
        }

        private void SendMsg(string txt, bool clean = false)
        {
            textBox1.BeginInvoke(new Action(() =>
            {
                if (clean)
                    textBox1.Text = AddText(txt);
                else
                    textBox1.Text += AddText(txt);
            }));
        }
        private string AddText(string txt)
        {
            string s = string.Format("{0:yyyy-MM-dd} {0:HH:mm:ss.fff} | {1}\r\n", DateTime.Now, txt);
            return s;
        }
    }
}
