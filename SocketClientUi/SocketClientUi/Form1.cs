﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SocketClientUi
{
    public partial class Form1 : Form
    {
        Socket clientSocket = null;
        static bool isListen = true;
        Thread thDataFromServer;
        IPAddress ipadr;
        public Form1()
        {
            InitializeComponent();
            ipadr = IPAddress.Loopback;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBox4.Text.Trim()))
            {
                MessageBox.Show("you cant't input null string");
                return;
            }

            if (clientSocket != null && clientSocket.Connected)
            {
                // 預設傳送 msg + '$'
                byte[] bytesSend = Encoding.UTF8.GetBytes(textBox4.Text + "$");
                clientSocket.Send(bytesSend);
                textBox4.Text = "";
            }
            else
            {
                MessageBox.Show("no connecting with server");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBox1.Text.Trim()))
            {
                MessageBox.Show("please input your id");
                return;
            }

            if (textBox1.Text.ToString().Trim().Equals("Server has closed"))
            {
                MessageBox.Show("you can't use this id");
                return;
            }

            if (clientSocket == null || !clientSocket.Connected)
            {
                try
                {
                    clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    if (!String.IsNullOrWhiteSpace(textBox2.Text.ToString().Trim()))
                    {
                        try
                        {
                            ipadr = IPAddress.Parse(textBox2.Text.ToString().Trim());
                        }
                        catch
                        {
                            MessageBox.Show("please input correct ip address");
                            return;
                        }
                    }
                    else
                    {
                        ipadr = IPAddress.Loopback;
                    }

                    clientSocket.BeginConnect(ipadr, 9487, (args) =>
                    {
                        if (args.IsCompleted)
                        {
                            byte[] bytesSend = new byte[4096];

                            textBox1.BeginInvoke(new Action(() =>
                            {
                                // 連線完成後 傳送 id + '$'
                                bytesSend = Encoding.UTF8.GetBytes(textBox1.Text.Trim() + "$");

                                if (clientSocket != null && clientSocket.Connected)
                                {
                                    clientSocket.Send(bytesSend);
                                    textBox1.Enabled = false;

                                    thDataFromServer = new Thread(DataFromServer);
                                    thDataFromServer.IsBackground = true;
                                    thDataFromServer.Start();
                                }
                                else
                                {
                                    MessageBox.Show("服务器已关闭");
                                }

                            }));

                            textBox2.BeginInvoke(new Action(() =>
                            {
                                if (clientSocket != null && clientSocket.Connected)
                                {
                                    textBox2.Enabled = false;
                                }
                            }));
                        }
                    }, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            else
            {
                MessageBox.Show("you already connect to the server");
            }
        }

        private void DataFromServer()
        {
            SendMsg("Connected to the Server.");

            isListen = true;
            try
            {
                while (isListen)
                {
                    byte[] bytesFrom = new byte[4096];
                    int len = clientSocket.Receive(bytesFrom);

                    String dataFromClient = Encoding.UTF8.GetString(bytesFrom, 0, len);

                    if (!String.IsNullOrWhiteSpace(dataFromClient))
                    {

                        if (dataFromClient.ToString().Equals("Server has closed"))
                        {
                            SendMsg("Server close.");

                            textBox1.BeginInvoke(new Action(() =>
                            {
                                textBox1.Enabled = true;
                            }));   

                            textBox2.BeginInvoke(new Action(() =>
                            {

                                textBox2.Enabled = true;

                            }));

                            clientSocket.Close();
                            clientSocket = null;
                            isListen = false;
                            thDataFromServer.Abort();   //这一句必须放在最后，不然这个进程都关了后面的就不会执行了

                            return;
                        }


                        if (dataFromClient.StartsWith("#") && dataFromClient.EndsWith("#"))
                        {
                            String userName = dataFromClient.Substring(1, dataFromClient.Length - 2);

                            this.BeginInvoke(new Action(() =>
                            {
                                MessageBox.Show("用户名：[" + userName + "]已经存在，请尝试其他用户名并重试");

                            }));

                            textBox1.BeginInvoke(new Action(() =>
                            {
                                textBox1.Enabled = true;


                            }));

                            textBox2.BeginInvoke(new Action(() =>
                            {

                                textBox2.Enabled = true;

                            }));

                            clientSocket.Close();
                            clientSocket = null;
                            isListen = false;
                            thDataFromServer.Abort();

                            return;
                        }
                        else
                        {
                            SendMsg(dataFromClient);
                        }
                    }
                }
            }
            catch (SocketException ex)
            {
                isListen = false;
                if (clientSocket != null && clientSocket.Connected)
                {
                    //没有在客户端关闭连接，而是给服务器发送一个消息，在服务器端关闭连接
                    //这样可以将异常的处理放到服务器。客户端关闭会让客户端和服务器都抛异常
                    clientSocket.Send(Encoding.UTF8.GetBytes("$"));
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            textBox4.Focus();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // 結束的時候送一個結束字串[$]
            if (clientSocket != null && clientSocket.Connected)
            {
                clientSocket.Send(Encoding.UTF8.GetBytes("$"));
            }

            if (clientSocket!= null)
                clientSocket.Close();
            clientSocket = null;

            isListen = false;

            if (thDataFromServer != null)
                thDataFromServer.Abort();
            thDataFromServer = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (clientSocket != null && clientSocket.Connected)
            {
                thDataFromServer.Abort();
                thDataFromServer = null;

                // 結束的時候送一個結束字串[$]
                clientSocket.Send(Encoding.UTF8.GetBytes("$"));

                clientSocket.Close();
                clientSocket = null;

                SendMsg("Close connect with server.");

                textBox1.BeginInvoke(new Action(() =>
                {
                    textBox1.Enabled = true;
                }));

                textBox2.BeginInvoke(new Action(() =>
                {

                    textBox2.Enabled = true;

                }));
            }
        }

        public void SendMsg(string txt)
        {
            textBox3.BeginInvoke(new Action(() =>
            {
                textBox3.AppendText(txt + Environment.NewLine);

            }));
        }
    }
}
